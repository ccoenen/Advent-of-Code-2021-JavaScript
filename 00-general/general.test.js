import { strictEqual } from 'assert';
import { describe, it } from 'mocha';

import { loadRelativeFile } from './general.js';

describe('00-general', () => {
	it('can load files', async () => {
		const content = await loadRelativeFile(import.meta.url, 'fixtures/demo.txt');
		const expected = 'demo\n';
		strictEqual(content, expected);
	});
});
