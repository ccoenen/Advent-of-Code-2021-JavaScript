import fs from 'fs/promises';
import { dirname, join } from 'path';
import { fileURLToPath } from 'url';

export async function loadRelativeFile(url, name) {
	const __dirname = dirname(fileURLToPath(url));
	return await fs.readFile(join(__dirname, name), 'utf-8');
}
