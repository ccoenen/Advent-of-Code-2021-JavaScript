import { deepStrictEqual, strictEqual } from 'assert';

import { describe, it } from 'mocha';

import { loadRelativeFile } from '../00-general/general.js';


import demoInputParsed from './07-example.js';
import { findFuelMinimum, parse, travelToPosition } from './07-modules.js';

const demoInput = await loadRelativeFile(import.meta.url, '07-example.txt');

describe('07-module', () => {
	it('parses the input', async () => {
		const actual = parse(demoInput);
		deepStrictEqual(actual, demoInputParsed);
	});

	it('can calculate neccessary fuel for a desired position part 1', () => {
		const crabs = [].concat(demoInputParsed);
		const comparisons = [
			{position: 1, fuel: 41},
			{position: 2, fuel: 37},
			{position: 3, fuel: 39},
			{position: 10, fuel: 71}
		];
		for (let comparison of comparisons) {
			const actualFuel = travelToPosition(crabs, comparison.position, false);
			deepStrictEqual(actualFuel, comparison.fuel);
		}
	});

	it('can calculate neccessary fuel for a desired position part 2', () => {
		const crabs = [].concat(demoInputParsed);
		const comparisons = [
			{position: 2, fuel: 206},
			{position: 5, fuel: 168}
		];
		for (let comparison of comparisons) {
			const actualFuel = travelToPosition(crabs, comparison.position, true);
			deepStrictEqual(actualFuel, comparison.fuel);
		}
	});

	it('can find the best possible position on its own part 1', () => {
		const crabs = [].concat(demoInputParsed);
		const result = findFuelMinimum(crabs);
		strictEqual(result.position, 2);
		strictEqual(result.fuel, 37);
	});

	it('can find the best possible position on its own part 2', () => {
		const crabs = [].concat(demoInputParsed);
		const result = findFuelMinimum(crabs, true);
		strictEqual(result.position, 5);
		strictEqual(result.fuel, 168);
	});
});
