import { loadRelativeFile } from '../00-general/general.js';
import { findFuelMinimum, getCompoundingFuelAmount, parse } from './07-modules.js';

const input = await loadRelativeFile(import.meta.url, '07-input.txt');
const crabs = parse(input);
const result = findFuelMinimum(crabs);
console.log(`(Part 1) minimum fuel at position ${result.position} (${result.fuel} units)`);

const result2 = findFuelMinimum(crabs, true);
console.log(`(Part 2) minimum fuel at position ${result2.position} (${result2.fuel} units)`);
console.log(`(compounding fuel lookup table was hit ${getCompoundingFuelAmount.cacheHit} out of ${getCompoundingFuelAmount.cacheHit+getCompoundingFuelAmount.cacheMiss} times - ${getCompoundingFuelAmount.cacheHit/(getCompoundingFuelAmount.cacheHit+getCompoundingFuelAmount.cacheMiss)*100}%)`);
