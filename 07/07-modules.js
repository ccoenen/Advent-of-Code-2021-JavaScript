export function parse(content) {
	const bits = content.trim().split(',');
	return bits.map((b) => parseInt(b, 10));
}

const fuelLookupTable = {};

export function getCompoundingFuelAmount(steps) {
	// eslint-disable-next-line no-prototype-builtins
	if (fuelLookupTable.hasOwnProperty(steps)) {
		getCompoundingFuelAmount.cacheHit++;
		return fuelLookupTable[steps];
	}
	getCompoundingFuelAmount.cacheMiss++;
	let fuel = 0;
	for (let i = 1; i <= steps; i++) {
		fuel += i;
	}
	fuelLookupTable[steps] = fuel;
	return fuel;
}
getCompoundingFuelAmount.cacheHit = 0;
getCompoundingFuelAmount.cacheMiss = 0;

export function travelToPosition(crabs, desiredPosition, compoundingFuelCosts) {
	let fuel;
	if (!compoundingFuelCosts) {
		fuel = crabs.map(crabPosition => Math.abs(crabPosition - desiredPosition));
	} else {
		fuel = crabs.map(crabPosition => getCompoundingFuelAmount(Math.abs(crabPosition - desiredPosition)));
	}
	return fuel.reduce((previous, current) => previous+current); // sums all fuels
}

export function findFuelMinimum(crabs, compoundingFuelCosts) {
	const upperBound = Math.max(...crabs);
	let bestFuel = Infinity;
	let bestPosition = -1;
	for (let i = 0; i < upperBound; i++) {
		const fuel = travelToPosition(crabs, i, compoundingFuelCosts);
		if (fuel < bestFuel) {
			bestFuel = fuel;
			bestPosition = i;
		}
	}
	return {fuel: bestFuel, position: bestPosition};
}
