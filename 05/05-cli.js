import { loadRelativeFile } from '../00-general/general.js';
import { diagonalToMap, findExtent, isDiagonal, isHorizontal, isVertical, lineToMap, parse, prepareMap } from './05-modules.js';

const input = await loadRelativeFile(import.meta.url, '05-input.txt');
const { lines } = parse(input);
const extent = findExtent(lines);

console.log(`map has an extent of ${JSON.stringify(extent)}`);
const map = prepareMap(extent.x, extent.y);
const map2 = prepareMap(extent.x, extent.y);

for (let line of lines) {
	if (isHorizontal(line) || isVertical(line)) {
		lineToMap(line, map);
		lineToMap(line, map2);
	}
	if (isDiagonal(line)) {
		diagonalToMap(line, map2);
	}
}
const filtered = map.flat().filter((field) => {
	return field > 1;
});
const filtered2 = map2.flat().filter((field) => {
	return field > 1;
});

console.log(`${filtered.length} fields have at least two straight lines intersecting`);
console.log(`${filtered2.length} fields have at least two lines intersecting`);
