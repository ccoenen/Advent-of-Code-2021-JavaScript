export function parse(content) {
	const items = content.split('\n');
	const output = [];
	for (let item of items) {
		if (!item) {
			continue;
		}
		const line = {};
		const parts = item.trim().split(' -> ');
		const from = parts[0].split(',');
		const to = parts[1].split(',');

		line.from = {x: parseInt(from[0], 10), y: parseInt(from[1], 10)};
		line.to = {x: parseInt(to[0], 10), y: parseInt(to[1], 10)};

		output.push(line);
	}
	return {lines: output};
}

export function isHorizontal(line) {
	return line.from.y === line.to.y;
}

export function isVertical(line) {
	return line.from.x === line.to.x;
}

export function isDiagonal(line) {
	return Math.abs(line.from.x - line.to.x) === Math.abs(line.from.y - line.to.y);
}

export function findExtent(lines) {
	let x = 0;
	let y = 0;
	for (let line of lines) {
		x = Math.max(x, line.from.x, line.to.x);
		y = Math.max(y, line.from.y, line.to.y);
	}
	return {x: x + 1, y: y + 1};
}

export function prepareMap(maxX, maxY) {
	const map = [];
	for (let y = 0; y < maxY; y++) {
		map[y] = [];
		for (let x = 0; x < maxX; x++) {
			map[y][x] = 0;
		}
	}
	return map;
}

export function lineToMap(line, map) {
	const fromX = Math.min(line.from.x, line.to.x);
	const toX = Math.max(line.from.x, line.to.x);
	const fromY = Math.min(line.from.y, line.to.y);
	const toY = Math.max(line.from.y, line.to.y);

	for (let x = fromX; x <= toX; x++) {
		for (let y = fromY; y <= toY; y++) {
			map[y][x]++;
		}
	}
}

export function diagonalToMap(line, map) {
	const fromX = Math.min(line.from.x, line.to.x);
	const toX = Math.max(line.from.x, line.to.x);

	const steps = toX - fromX + 1;
	const xDirection = line.from.x > line.to.x ? -1 : 1;
	const yDirection = line.from.y > line.to.y ? -1 : 1;

	let {x, y} = line.from;

	for (let i = 0; i < steps; i++) {
		map[y][x]++;
		x += xDirection;
		y += yDirection;
	}
}

export function formatMap(map) {
	return map.map((row) => {
		return row.join('');
	}).join('\n');
}
