import assert, { deepStrictEqual, strictEqual } from 'assert';

import { describe, it } from 'mocha';

import { loadRelativeFile } from '../00-general/general.js';


import demoInputParsed from './05-example.js';
import { diagonalToMap, findExtent, formatMap, isDiagonal, isHorizontal, isVertical, lineToMap, parse, prepareMap } from './05-modules.js';

const demoInput = await loadRelativeFile(import.meta.url, '05-example.txt');

describe('05-module', () => {
	it('parses the input', async () => {
		const actual = parse(demoInput);
		deepStrictEqual(actual, demoInputParsed);
	});

	it('recognizes a horizontal and vertical line', () => {
		assert(isHorizontal({from: {x: 12, y: 0}, to: {x: 15, y: 0}}));
		assert(!isHorizontal({from: {x: 12, y: 0}, to: {x: 15, y: 2}}));

		assert(isVertical({from: {x: 22, y: 77}, to: {x: 22, y: 700}}));
		assert(!isVertical({from: {x: 22, y: 77}, to: {x: 23, y: 700}}));
	});

	it('recognizes a diagonal line', () => {
		assert(!isDiagonal({from: {x: 12, y: 0}, to: {x: 15, y: 0}}));
		assert(!isDiagonal({from: {x: 12, y: 0}, to: {x: 15, y: 2}}));
		assert(!isDiagonal({from: {x: 22, y: 77}, to: {x: 22, y: 700}}));
		assert(!isDiagonal({from: {x: 22, y: 77}, to: {x: 23, y: 700}}));

		assert(isDiagonal({from: {x: 22, y: 77}, to: {x: 25, y: 80}}));
		assert(isDiagonal({from: {x: 22, y: 77}, to: {x: 25, y: 74}}));
		assert(isDiagonal({from: {x: 22, y: 77}, to: {x: 19, y: 80}}));
		assert(isDiagonal({from: {x: 22, y: 77}, to: {x: 19, y: 74}}));
	});

	it('finds the extent of a map', () => {
		const extent = findExtent(demoInputParsed.lines);
		deepStrictEqual(extent, {x: 10, y: 10});
	});

	it('prepares an empty map', () => {
		const map = prepareMap(3, 4);
		const expected = [
			[0, 0, 0],
			[0, 0, 0],
			[0, 0, 0],
			[0, 0, 0]
		];
		deepStrictEqual(map, expected);
	});

	it('adds straight lines to a map', () => {
		const map = prepareMap(10, 10);
		const expectedMap = [
			[0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
			[0, 0, 1, 0, 0, 0, 0, 1, 0, 0],
			[0, 0, 1, 0, 0, 0, 0, 1, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
			[0, 1, 1, 2, 1, 1, 1, 2, 1, 1],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[2, 2, 2, 1, 1, 1, 0, 0, 0, 0]
		];

		for (let line of demoInputParsed.lines) {
			if (isHorizontal(line) || isVertical(line)) {
				lineToMap(line, map);
			}
		}

		deepStrictEqual(map, expectedMap);
	});

	it('adds ALL lines to a map', () => {
		const map = prepareMap(10, 10);
		const expectedMap = [
			[1, 0, 1, 0, 0, 0, 0, 1, 1, 0],
			[0, 1, 1, 1, 0, 0, 0, 2, 0, 0],
			[0, 0, 2, 0, 1, 0, 1, 1, 1, 0],
			[0, 0, 0, 1, 0, 2, 0, 2, 0, 0],
			[0, 1, 1, 2, 3, 1, 3, 2, 1, 1],
			[0, 0, 0, 1, 0, 2, 0, 0, 0, 0],
			[0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
			[0, 1, 0, 0, 0, 0, 0, 1, 0, 0],
			[1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
			[2, 2, 2, 1, 1, 1, 0, 0, 0, 0]
		];

		for (let line of demoInputParsed.lines) {
			if (isHorizontal(line) || isVertical(line)) {
				lineToMap(line, map);
			}
			if (isDiagonal(line)) {
				diagonalToMap(line, map);
			}
		}

		strictEqual(formatMap(map), formatMap(expectedMap));
		deepStrictEqual(map, expectedMap);
	});

});
