import { deepStrictEqual, strictEqual } from 'assert';

import { describe, it } from 'mocha';

import { loadRelativeFile } from '../00-general/general.js';


import demoInputParsed from './11-example.js';
import { format, parse, tick } from './11-modules.js';

const demoInput = await loadRelativeFile(import.meta.url, '11-example.txt');

describe('11-module', () => {
	it('parses the input', async () => {
		const actual = parse(demoInput);
		deepStrictEqual(actual, demoInputParsed);
	});

	it('evolves the way it is described in the docs', () => {
		const map = parse(demoInput);
		const expected = [
			parse('5483143223\n2745854711\n5264556173\n6141336146\n6357385478\n4167524645\n2176841721\n6882881134\n4846848554\n5283751526'),
			parse('6594254334\n3856965822\n6375667284\n7252447257\n7468496589\n5278635756\n3287952832\n7993992245\n5957959665\n6394862637'),
			parse('8807476555\n5089087054\n8597889608\n8485769600\n8700908800\n6600088989\n6800005943\n0000007456\n9000000876\n8700006848'),
			parse('0050900866\n8500800575\n9900000039\n9700000041\n9935080063\n7712300000\n7911250009\n2211130000\n0421125000\n0021119000'),
			parse('2263031977\n0923031697\n0032221150\n0041111163\n0076191174\n0053411122\n0042361120\n5532241122\n1532247211\n1132230211'),
			parse('4484144000\n2044144000\n2253333493\n1152333274\n1187303285\n1164633233\n1153472231\n6643352233\n2643358322\n2243341322'),
			parse('5595255111\n3155255222\n3364444605\n2263444496\n2298414396\n2275744344\n2264583342\n7754463344\n3754469433\n3354452433'),
			parse('6707366222\n4377366333\n4475555827\n3496655709\n3500625609\n3509955566\n3486694453\n8865585555\n4865580644\n4465574644'),
			parse('7818477333\n5488477444\n5697666949\n4608766830\n4734946730\n4740097688\n6900007564\n0000009666\n8000004755\n6800007755'),
			parse('9060000644\n7800000976\n6900000080\n5840000082\n5858000093\n6962400000\n8021250009\n2221130009\n9111128097\n7911119976'),
			parse('0481112976\n0031112009\n0041112504\n0081111406\n0099111306\n0093511233\n0442361130\n5532252350\n0532250600\n0032240000')
		];

		let flashSum = 0;

		deepStrictEqual(map, expected[0]);
		flashSum += tick(map);
		deepStrictEqual(map, expected[1]);
		flashSum += tick(map);
		deepStrictEqual(map, expected[2]);
		flashSum += tick(map);
		deepStrictEqual(map, expected[3]);
		flashSum += tick(map);
		deepStrictEqual(map, expected[4]);
		flashSum += tick(map);
		deepStrictEqual(map, expected[5]);
		flashSum += tick(map);
		deepStrictEqual(map, expected[6]);
		flashSum += tick(map);
		deepStrictEqual(map, expected[7]);
		flashSum += tick(map);
		deepStrictEqual(map, expected[8]);
		flashSum += tick(map);
		deepStrictEqual(map, expected[9]);
		flashSum += tick(map);
		deepStrictEqual(map, expected[10]);

		strictEqual(flashSum, 204);
	});

	it('tick can rollover a otherwise unimpressive point', () => {
		const map = [
			[1,1,1,1,1],
			[1,9,9,9,1],
			[1,9,1,9,1],
			[1,9,9,9,1],
			[1,1,1,1,1],
		];

		const expected1 = [
			[3,4,5,4,3],
			[4,0,0,0,4],
			[5,0,0,0,5],
			[4,0,0,0,4],
			[3,4,5,4,3]
		];

		const expected2 = [
			[4,5,6,5,4],
			[5,1,1,1,5],
			[6,1,1,1,6],
			[5,1,1,1,5],
			[4,5,6,5,4]
		];

		const flashes = tick(map);
		deepStrictEqual(format(map), format(expected1));
		strictEqual(flashes, 9);
		tick(map);
		deepStrictEqual(format(map), format(expected2));
	});

});
