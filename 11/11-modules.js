export function parse(input) {
	return input.trim().split('\n').map(line => line.split('').map(char => parseInt(char,10)));
}

/**
 * extra mutation for each flash at x0,y0.
 */
export function checkFlash(map, x0, y0, recursionDepth = 0) {
	// const prefix = ' '.repeat(recursionDepth*4);
	const ROWS = map.length;
	const COLS = map[0].length;

	if (map[y0][x0] === 10) {
		// console.log(`${prefix}${x0},${y0} flashed, now processing 3x3 matrix`);
		for (let y = Math.max(0, y0 - 1); y < Math.min(ROWS, y0 + 2); y++) {
			for (let x = Math.max(0, x0 - 1); x < Math.min(COLS, x0 + 2); x++) {
				// console.log(`${prefix}-- ${x},${y}`);
				map[y][x]++;
				checkFlash(map, x, y, recursionDepth + 1);
			}
		}
	}
}

/**
 * mutates the map
 * 
 */
export function tick(map) {
	const ROWS = map.length;
	const COLS = map[0].length;
	let flashes = 0;

	for (let y = 0; y < ROWS; y++) {
		for (let x = 0; x < COLS; x++) {
			map[y][x]++;
			checkFlash(map, x, y);
		}
	}

	for (let y = 0; y < ROWS; y++) {
		for (let x = 0; x < COLS; x++) {
			if (map[y][x] > 9) {
				flashes++;
				map[y][x] = 0;
			}
		}
	}



	return flashes;
}

export function format(map) {
	return map.map(line => line.join('')).join('\n');
}
