import { loadRelativeFile } from '../00-general/general.js';
import { parse, tick } from './11-modules.js';

const map = parse(await loadRelativeFile(import.meta.url, '11-input.txt'));
let flashes = 0;
for (let i = 0; i < 100; i++) {
	flashes += tick(map);
}
console.log(`${flashes} flashes after 100 steps`);

const map2 = parse(await loadRelativeFile(import.meta.url, '11-input.txt'));

let counter = 0;
while (tick(map2) !== 100) {
	counter++;
}
console.log(`fully synchronized after ${counter+1} ticks`);
