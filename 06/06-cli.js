import { loadRelativeFile } from '../00-general/general.js';
import { bucketTick, parse, schoolToBucket, tick } from './06-modules.js';

const input = await loadRelativeFile(import.meta.url, '06-input.txt');
const school = parse(input);
for (let dayCounter = 1; dayCounter <= 80; dayCounter++) {
	tick(school);
}
console.log(`On Day 80, there are ${school.length} fish`);

const school2 = parse(input);
const bucket = schoolToBucket(school2);
for (let dayCounter = 1; dayCounter <= 256; dayCounter++) {
	bucketTick(bucket);
}

const values = Object.values(bucket);
const sum = values.reduce(((prev, current) => parseInt(prev, 10)+parseInt(current, 10)));
console.log(`On Day 256, there are ${sum} fish`);
