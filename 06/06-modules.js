const INITIAL_VALUE = 8;
const WRAP_AROUND_VALUE = 6;

export function parse(content) {
	const bits = content.split(',');
	const numbers = bits.map(b => parseInt(b, 10));
	return numbers;
}

export function tick(school) {
	for (let i = school.length - 1; i >= 0; i--) { //  counting backwards because we add stuff at the end!
		school[i] = school[i] - 1;
		if (school[i] === -1) {
			school[i] = WRAP_AROUND_VALUE;
			school.push(INITIAL_VALUE);
		}
	}
}

export function schoolToBucket(school) {
	const output = {};
	for (let fish of school) {
		output[fish] = output[fish] || 0;
		output[fish]++;
	}
	return output;
}

export function bucketTick(bucket) {
	for (let i = -1; i < 8; i++) {
		bucket[i] = bucket[i+1] || 0;
	}
	bucket[INITIAL_VALUE] = bucket[-1];
	bucket[WRAP_AROUND_VALUE] = bucket[WRAP_AROUND_VALUE] + bucket[-1];
	delete bucket[-1];

	for (let i = 0; i <= 8; i++) {
		if (!bucket[i]) delete bucket[i];
	}
}
