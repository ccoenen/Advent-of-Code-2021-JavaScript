import { deepStrictEqual } from 'assert';

import { describe, it } from 'mocha';

import { loadRelativeFile } from '../00-general/general.js';


import demoInputParsed from './06-example.js';
import { bucketTick, parse, schoolToBucket, tick } from './06-modules.js';

const demoInput = await loadRelativeFile(import.meta.url, '06-example.txt');

describe('06-module', () => {
	it('parses the input', async () => {
		const actual = parse(demoInput);
		deepStrictEqual(actual, demoInputParsed);
	});

	it('creates buckets for a given list of fish', () => {
		const input = [1, 3, 3, 3, 5];
		const expected = {
			1: 1,
			3: 3,
			5: 1
		};
		deepStrictEqual(schoolToBucket(input), expected);
	});

	it('can do ticks like in the example', () => {
		const school = [].concat(demoInputParsed);
		tick(school);
		deepStrictEqual(school, [2,3,2,0,1]);
		tick(school);
		deepStrictEqual(school, [1,2,1,6,0,8]);
		tick(school);
		deepStrictEqual(school, [0,1,0,5,6,7,8]);
		tick(school);
		deepStrictEqual(school, [6,0,6,4,5,6,7,8,8]);
		tick(school);
		deepStrictEqual(school, [5,6,5,3,4,5,6,7,7,8]);
		tick(school);
		deepStrictEqual(school, [4,5,4,2,3,4,5,6,6,7]);
		tick(school);
		deepStrictEqual(school, [3,4,3,1,2,3,4,5,5,6]);
		tick(school);
		deepStrictEqual(school, [2,3,2,0,1,2,3,4,4,5]);
		tick(school);
		deepStrictEqual(school, [1,2,1,6,0,1,2,3,3,4,8]);
		tick(school);
		deepStrictEqual(school, [0,1,0,5,6,0,1,2,2,3,7,8]);
		tick(school);
		deepStrictEqual(school, [6,0,6,4,5,6,0,1,1,2,6,7,8,8,8]);
		tick(school);
		deepStrictEqual(school, [5,6,5,3,4,5,6,0,0,1,5,6,7,7,7,8,8]);
		tick(school);
		deepStrictEqual(school, [4,5,4,2,3,4,5,6,6,0,4,5,6,6,6,7,7,8,8]);
		tick(school);
		deepStrictEqual(school, [3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8]);
		tick(school);
		deepStrictEqual(school, [2,3,2,0,1,2,3,4,4,5,2,3,4,4,4,5,5,6,6,7]);
		tick(school);
		deepStrictEqual(school, [1,2,1,6,0,1,2,3,3,4,1,2,3,3,3,4,4,5,5,6,8]);
		tick(school);
		deepStrictEqual(school, [0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8]);
		tick(school);
		deepStrictEqual(school, [6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8]);
	});

	it('can solve the fish generations with buckets', () => {
		const school = [].concat(demoInputParsed);
		const bucket = schoolToBucket(school);
		bucketTick(bucket);
		deepStrictEqual(bucket, {
			0: 1,
			1: 1,
			2: 2,
			3: 1
		});
		for (let i = 1; i < 18; i++) { // 1 because we already did a single tick!
			bucketTick(bucket);
		}
		deepStrictEqual(bucket, {
			0: 3,
			1: 5,
			2: 3,
			3: 2,
			4: 2,
			5: 1,
			6: 5,
			7: 1,
			8: 4
		});
	});
});


