export function parse(map) {
	const lines = map.trim().split('\n');
	return lines.map((l) => l.split('').map(char => parseInt(char, 10)));
}

export function getLowPoints(map) {
	const ROWS = map.length;
	const COLS = map[0].length;
	const lowPoints = [];

	for (let y = 0; y < ROWS; y++) {
		for (let x = 0; x < COLS; x++) {
			const height = map[y][x];
			if (
				(x === 0 || height < map[y][x-1]) &&
				(x === (COLS - 1) || height < map[y][x+1]) &&
				(y === 0 || height < map[y-1][x]) &&
				(y === (ROWS - 1) || height < map[y+1][x])
			) {
				lowPoints.push({x, y, height});
			}
		}
	}

	return lowPoints;
}

export function walkBasin(map, startX, startY, marker = []) {
	marker[startY] = marker[startY] || [];
	if (marker[startY][startX]) return; // we've been here before
	marker[startY][startX] = true; // have we?

	if (map[startY][startX] === 9) return;

	const basin = [];
	if (startY > 0) {
		basin.push(walkBasin(map, startX, startY - 1, marker));
	}
	if (startY < map.length - 2) {
		basin.push(walkBasin(map, startX, startY + 1, marker));
	}
	if (startX > 0) {
		basin.push(walkBasin(map, startX - 1, startY, marker));
	}
	if (startX < map[0].length - 2) {
		basin.push(walkBasin(map, startX + 1, startY, marker));
	}

	basin.push({ x: startX, y: startY, height: map[startY][startX] });

	return basin.filter((v) => !!v).flat();
}
