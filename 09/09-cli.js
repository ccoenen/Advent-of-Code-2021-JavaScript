import { loadRelativeFile } from '../00-general/general.js';
import { parse, getLowPoints, walkBasin } from './09-modules.js';

const input = parse(await loadRelativeFile(import.meta.url, '09-input.txt'));
const points = getLowPoints(input);
console.log(`${points.reduce((prev,curr) => prev+curr.height, 0) + points.length} is the sum of all risks.`);

const basins = points.map(p => {
	const basinPoints = walkBasin(input, p.x, p.y);
	return basinPoints.length;
});

basins.sort((a,b) => b-a);
console.log(`largest three product: ${basins[0] * basins[1] * basins[2]}`);
