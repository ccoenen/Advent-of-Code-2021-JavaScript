import { deepStrictEqual, strictEqual } from 'assert';

import { describe, it } from 'mocha';

import { loadRelativeFile } from '../00-general/general.js';


import demoInputParsed from './09-example.js';
import { getLowPoints, parse, walkBasin } from './09-modules.js';

const demoInput = await loadRelativeFile(import.meta.url, '09-example.txt');

describe('09-module', () => {
	it('parses the input', async () => {
		const actual = parse(demoInput);
		deepStrictEqual(actual, demoInputParsed);
	});

	it('finds the low spots for example data', () => {
		const lp = getLowPoints(demoInputParsed);
		deepStrictEqual(lp, [
			{ height: 1, x: 1, y: 0 },
			{ height: 0, x: 9, y: 0 },
			{ height: 5, x: 2, y: 2 },
			{ height: 5, x: 6, y: 4 }
		]);
	});

	it('can walk the basin 2', () => {
		const points = walkBasin(demoInputParsed, 9, 0);
		strictEqual(points.length, 9);
	});
});
