import { deepStrictEqual, strictEqual } from 'assert';
import { assert } from 'console';

import { describe, it } from 'mocha';

import { loadRelativeFile } from '../00-general/general.js';


import demoInputParsed from './10-example.js';
import { isCorrupted, parse, CORRUPTION_POINT_MAPPING, remainingStack, INCOMPLETE_POINT_MAPPING } from './10-modules.js';

const demoInput = await loadRelativeFile(import.meta.url, '10-example.txt');

describe('10-module', () => {
	it('parses the input', async () => {
		const actual = parse(demoInput);
		deepStrictEqual(actual, demoInputParsed);
	});

	it('recognizes a corrupted chunk', () => {
		const legal = [
			'()',
			'[]',
			'([])',
			'{()()()}',
			'<([{}])>',
			'[<>({}){}[([])<>]]',
			'(((((((((())))))))))'
		];
		const corrupted = [
			'(]',
			'{()()()>',
			'(((()))}',
			'<([]){()}[{}])'
		];

		for (let ok of legal) {
			strictEqual(isCorrupted(ok), false, `${ok} should NOT register as corrupt`);
		}

		for (let failed of corrupted) {
			assert(isCorrupted(failed), `${failed} SHOULD register as corrupt`);
		}
	});

	it('finds the corrupt lines form the example and calculates score', () => {
		const result = demoInputParsed.map(isCorrupted);
		const points = result.map(character => CORRUPTION_POINT_MAPPING[character] || 0);
		const sum = points.reduce((prev, curr) => prev+curr);
		strictEqual(sum, 26397);
	});

	it('knows how to complete an incomplete line', () => {
		const incomplete = demoInputParsed.filter(line => !isCorrupted(line));
		const stacks = incomplete.map(remainingStack);
		const points = stacks.map(stack => {
			return stack.reduce((prev, current) => {return prev*5 + INCOMPLETE_POINT_MAPPING[current];}, 0);
		});
		deepStrictEqual(points, [288957, 5566, 1480781, 995444, 294]);
	});
});
