import { loadRelativeFile } from '../00-general/general.js';
import { isCorrupted, parse, CORRUPTION_POINT_MAPPING, remainingStack, INCOMPLETE_POINT_MAPPING } from './10-modules.js';

const input = parse(await loadRelativeFile(import.meta.url, '10-input.txt'));
const corruptedLines = input.map(isCorrupted);
const points = corruptedLines.map(character => CORRUPTION_POINT_MAPPING[character] || 0);
const sum = points.reduce((prev, curr) => prev+curr);
console.log(`Total of ${sum} points for parser errors!`);

const incomplete = input.filter(line => !isCorrupted(line));
const stacks = incomplete.map(remainingStack);
const incompletePoints = stacks.map(stack => {
	return stack.reduce((prev, current) => {return prev*5 + INCOMPLETE_POINT_MAPPING[current];}, 0);
});
incompletePoints.sort((a,b) => b-a);
console.log(`Median incomplete line stack score is ${incompletePoints[(incompletePoints.length - 1) / 2]}`);
