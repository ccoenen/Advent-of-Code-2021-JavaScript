export const CORRUPTION_POINT_MAPPING = {
	')': 3,
	']': 57,
	'}': 1197,
	'>': 25137
};

export const INCOMPLETE_POINT_MAPPING = {
	')': 1,
	']': 2,
	'}': 3,
	'>': 4
};

export const PAIRS = {
	'(': ')',
	'[': ']',
	'{': '}',
	'<': '>'
};

export function parse(input) {
	return input.trim().split('\n');
}

/**
 * @returns the illegal character for a line, in case it is corrupted. returns false if not corrupted.
 */
export function isCorrupted(chunk) {
	const analysis = analyze(chunk);
	return analysis.corruptCharacter || false;
}

export function remainingStack(chunk) {
	const analysis = analyze(chunk);
	if (analysis.incomplete) {
		return analysis.stack.reverse().map(char => {
			return PAIRS[char];
		});
	}
	return [];
}

export function analyze(chunk) {
	const stack = [];
	for (const index in chunk.split('')) {
		const element = chunk[index];
		// console.log(`Running with ${element} from index ${index}`);
		if (element === '(' || element === '[' || element === '{' || element === '<') {
			stack.push(element);
		} else if (element === ')' && stack[stack.length - 1] === '(') {
			stack.pop();
		} else if (element === ']' && stack[stack.length - 1] === '[') {
			stack.pop();
		} else if (element === '}' && stack[stack.length - 1] === '{') {
			stack.pop();
		} else if (element === '>' && stack[stack.length - 1] === '<') {
			stack.pop();
		} else {
			// console.log(`char ${index}: ${element} was not expected. looking for a match to ${stack[stack.length - 1]}`);
			return {corruptCharacter: element, chunk};
		}
	}

	// this would register incomplete lines:
	if (stack.length > 0) {
		return {incomplete: true, stack, chunk};
	}
	return false;
}
