# Advent of Code 2021 in JavaScript

After I dropped out early in previous years because I could not keep up while learning a new language, I am this year trying to do this as quickly as possible.

This also means that my code probably is _NOT_ a great example.


## Running tests for a single day

`npm run mocha -- 05/` will run the tests just for directory `05`. Can also be any other path.
