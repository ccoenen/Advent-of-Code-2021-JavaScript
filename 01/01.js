import { loadRelativeFile } from '../00-general/general.js';

const content = await loadRelativeFile(import.meta.url, '01-input.txt');
const values = content.split('\n');
const measurements = values.map(v => parseInt(v, 10));

// Part 1
function increaseCounter(measurements) {
	let previous;
	let increasedCounter = 0;
	for (const m of measurements) {
		if (previous && previous < m) {
			increasedCounter++;
		}
		previous = m;
	}
	return increasedCounter;
}

console.log(increaseCounter(measurements));

// Part 2
const slidingWindow = [];
for (let i = 0; i < measurements.length - 2; i++) {
	slidingWindow.push(measurements[i] + measurements[i+1] + measurements[i+2]);
}

console.log(increaseCounter(slidingWindow));
