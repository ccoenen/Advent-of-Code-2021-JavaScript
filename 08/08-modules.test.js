import { deepStrictEqual, strictEqual } from 'assert';

import { describe, it } from 'mocha';

import { loadRelativeFile } from '../00-general/general.js';


import demoInputParsed from './08-example.js';
import { applyMap, decode, findAllUniqueNumbers, findMap, findUniqueNumbers, parse } from './08-modules.js';

const demoInput = await loadRelativeFile(import.meta.url, '08-example.txt');

describe('08-module', () => {
	it('parses the input', async () => {
		const actual = parse(demoInput);
		deepStrictEqual(actual, demoInputParsed);
	});

	it('applies a map to an input', () => {
		const input = 'abcdef';
		const map = {
			a: 'x',
			b: 'y',
			c: 'l',
			d: 'a',
			e: 'o',
			f: 'q'
		};

		const output = applyMap(input, map);
		strictEqual(output, 'xylaoq');
	});

	it('can find a map based on a signal string', () => {
		const expectedMap = {
			a: 'c',
			b: 'f',
			c: 'g',
			d: 'a',
			e: 'b',
			f: 'd',
			g: 'e'
		};
		const result = findMap(demoInputParsed[0].signalPattern);
		deepStrictEqual(result, expectedMap);
	});

	it('long example checks out, too', async () => {
		const demoInput = parse(await loadRelativeFile(import.meta.url, '08-example-2.txt'));
		const numbers = demoInput.map(line => {
			return findUniqueNumbers(line.digitOutput);
		});
		const sum = numbers.reduce((prev, curr) => prev+curr);
		const expectedNumer = 26;
		strictEqual(sum, expectedNumer);

		const decodedNumbers = demoInput.map((input) => {
			const map = findMap(input.signalPattern);
			const decoded = decode(input.digitOutput, map);
			return decoded;
		});

		const expectedNumbers = [
			8394,
			9781,
			1197,
			9361,
			4873,
			8418,
			4548,
			1625,
			8717,
			4315,
		];
	
		deepStrictEqual(decodedNumbers, expectedNumbers);
	});

	it('can find unique digits 1, 4, 7 and 8 in a single call', async () => {
		const demoInput = parse(await loadRelativeFile(import.meta.url, '08-example-2.txt'));
		const sum = findAllUniqueNumbers(demoInput);
		const expectedNumer = 26;
		strictEqual(sum, expectedNumer);
	});

	it('can decode digitOutputs with a given map', () => {
		const map = findMap(demoInputParsed[0].signalPattern);
		const decoded = decode(demoInputParsed[0].digitOutput, map);
		const expected = 5353;
		strictEqual(decoded, expected);
	});

});
