/* eslint-disable quotes */
export default
[
	{
		"signalPattern": [
			"acedgfb",
			"cdfbe",
			"gcdfa",
			"fbcad",
			"dab",
			"cefabd",
			"cdfgeb",
			"eafb",
			"cagedb",
			"ab"
		],
		"digitOutput": [
			"cdfeb",
			"fcadb",
			"cdfeb",
			"cdbaf"
		]
	}
];
