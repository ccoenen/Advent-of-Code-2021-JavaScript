import { loadRelativeFile } from '../00-general/general.js';
import { decode, findAllUniqueNumbers, findMap, parse } from './08-modules.js';

const input = parse(await loadRelativeFile(import.meta.url, '08-input.txt'));
const sum = findAllUniqueNumbers(input);
console.log(`${sum} unique numbers in input`);

const decodedNumbers = input.map((input) => {
	const map = findMap(input.signalPattern);
	const decoded = decode(input.digitOutput, map);
	return decoded;
});

console.log(`sum of all numbers: ${decodedNumbers.reduce((prev, curr) => prev+curr)}`);
