const CORRECT_PINS = [
	'abcefg', // 0
	'cf',     // 1
	'acdeg',  // 2
	'acdfg',  // 3
	'bcdf',   // 4
	'abdfg',  // 5
	'abdefg', // 6
	'acf',    // 7
	'abcdefg',// 8
	'abcdfg', // 9
];

export function parse(content) {
	const lines = content.trim().split('\n');
	return lines.map(line => {
		const parts = line.split(/\s+\|\s+/);
		return {
			'signalPattern': parts[0].split(/\s+/),
			'digitOutput': parts[1].split(/\s+/)
		};
	});
}

export function applyMap(input, map) {
	return input.split('').map((char) => {
		return map[char] || char;
	}).join('');
}

export function findUniqueNumbers(digitOutput) {
	let counter = 0;
	for (let digit of digitOutput) {
		if (digit.length === 2 || digit.length === 4 || digit.length === 3 || digit.length === 7) {
			counter++;
		}
	}
	return counter;
}

export function findAllUniqueNumbers(parsedList) {
	const numbers = parsedList.map(line => {
		return findUniqueNumbers(line.digitOutput);
	});
	return numbers.reduce((prev, curr) => prev+curr);
}

/**
 * @param {Array} signalPattern from the riddle.
 * @returns {Object} mapping a found wire to the ACTUAL place it is in.
 *                   So `{ "a": "g" }` means: in this signalPattern,
 *                   a should be replaced with g.
 */
export function findMap(signalPattern) {
	const map = {};

	/// preparations
	// sorting signalPattern by length - we now know where every group is.
	signalPattern.sort((a, b) => a.length - b.length);

	// 1 will be in index 0 (only 2 wires lit up)
	// 7 will be in index 1 (only 3 wires lit up)
	// 4 will be in index 2 (only 4 wires lit up)
	// 2, 3 or 5 will be in index 3-5 (5 wires lit up)
	// 0, 6 or 9 will be in index 6-8 (6 wires lit up)
	// 8 will be in index 9 (all 7 wires lit up)

	// counting each letter
	const counts = {a:0, b:0, c:0, d:0, e:0, f:0, g:0};
	signalPattern.join('').split('').forEach(current => counts[current]++);

	// "a" - is in 8 out of 10 digits. - but only in 7 NOT in 1
	// "b" - is in **6** out of 10 digits.
	// "c" - it is in 8 out of 10 digits - and must be in BOTH 1 and 7
	// "d" - it is in 7 out of 10 digits, but not in 1 or 7.
	// "e" - in **4**
	// "f" - it is in **9** out of 10 digits - the remaining unknwon wire common to 1 and 7
	// "g" - is in 7 as well.


	// find "a" - the wire _NOT_ common to 1 and 7 (index 0 and 1)
	const aWire = signalPattern[1].replace(signalPattern[0][0], '').replace(signalPattern[0][1], '');
	map[aWire] = 'a';


	// finding all the other wires
	for (const [key, value] of Object.entries(counts)) {
		// the first three are easy: they have a unique count!
		if (value === 4) {
			map[key] = 'e';
		}
		if (value === 6) {
			map[key] = 'b';
		}
		if (value === 9) {
			map[key] = 'f';
		}

		if (value === 7) {
			if (signalPattern[2].indexOf(key) > -1) {
				// the one that's seven times that ALSO is in the digit 4 (which sorts to index 2)
				map[key] = 'd';
			} else {
				// the one that is _NOT_ in digit 4.
				map[key] = 'g';
			}
		}
		if (value === 8 && key !== aWire) {
			map[key] = 'c';
		}
	}

	return map;
}

export function decode(digits, map) {
	return digits.map((d) => {
		const corrected = d.split('').map(c => map[c]).sort().join('');
		return CORRECT_PINS.indexOf(corrected);
	}).reduce((prev, curr) => prev * 10 + curr);
}
