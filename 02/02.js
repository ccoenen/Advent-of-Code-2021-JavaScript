import { loadRelativeFile } from '../00-general/general.js';

const input = await loadRelativeFile(import.meta.url, '02-input.txt');

// here we actually start processing it:
const values = input.split('\n');
const instructions = values.map((v) => {
	const bits = v.split(' ');
	return {direction: bits[0], value: parseInt(bits[1], 10)};
});

const position = {horizontal: 0, depth: 0};
for (const instruction of instructions) {
	switch (instruction.direction) {
	case 'forward':
		position.horizontal += instruction.value;
		break;
	case 'down':
		position.depth += instruction.value;
		break;
	case 'up':
		position.depth -= instruction.value;
		break;
	case '':
		break;
	default:
		console.error('unknown direction: -' + instruction.direction + '-' + JSON.stringify(instruction));
		process.exit(1);
	}
}

console.log(position, position.horizontal*position.depth);


const position2 = {aim: 0, horizontal: 0, depth: 0};
for (const instruction of instructions) {
	switch (instruction.direction) {
	case 'forward':
		position2.horizontal += instruction.value;
		position2.depth += position2.aim * instruction.value;
		break;
	case 'down':
		position2.aim += instruction.value;
		break;
	case 'up':
		position2.aim -= instruction.value;
		break;
	case '':
		break;
	default:
		console.error('unknown direction: -' + instruction.direction + '-' + JSON.stringify(instruction));
		process.exit(1);
	}
}

console.log(position2, position2.horizontal*position2.depth);
