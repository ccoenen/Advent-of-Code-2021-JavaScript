import { loadRelativeFile } from '../00-general/general.js';
import { Board, parse } from './04-modules.js';


const input = await loadRelativeFile(import.meta.url, '04-input.txt');
const data = parse(input);
const boards = data.boards.map(b => new Board(b));

// part 1
let points = -1;
for (let drawnNumber of data.drawNumbers) {
	for (let board of boards) {
		if (points !== -1) { break; }
		board.mark(drawnNumber);
		if (board.hasWon()) {
			points = board.calculateScoreWith(drawnNumber);
			console.log(`Board ${board.toString()}\nwins with ${points} points`);
		}
	}
}

// for part 2 we can just continue with the boards, because the beginning is the same.
const winningOrder = [];
let finalDrawnNumber = -1;
for (let drawnNumber of data.drawNumbers) {
	for (let i in boards) {
		if (winningOrder.indexOf(i) !== -1) {
			continue;
		}
		let board = boards[i];
		board.mark(drawnNumber);
		if (board.hasWon()) {
			finalDrawnNumber = drawnNumber;
			winningOrder.push(i);
		}
	}
}

console.log(`Boards win in this order: ${winningOrder}, last one is ${winningOrder[winningOrder.length - 1]}`);

console.log(`Last board's winning score: ${boards[winningOrder[winningOrder.length - 1]].calculateScoreWith(finalDrawnNumber)}`);
