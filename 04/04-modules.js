export function parse(content) {
	const segments = content.split('\n\n');
	const drawNumbers = segments[0].split(',').map(n => parseInt(n, 10));
	const boards = [];

	for (let i = 1; i < segments.length; i++) {
		const lines = segments[i].split('\n');
		const board = [];
		for (let line of lines) {
			const fields = line.trim().split(/\s+/).map(n => parseInt(n, 10));
			const filteredFields = fields.filter(e => !isNaN(e));
			if (filteredFields.length > 0) {
				board.push(filteredFields);
			}
		}
		boards.push(board);
	}

	return {
		drawNumbers,
		boards
	};
}

export class Board {
	/**
	 * @param {Array} fields. 2d-array. Outer array is Y-rows. Inner Arry is X-columns
	 */
	constructor(fields) {
		this.fields = fields;
		this.marked = [];
		for (let f in fields) {
			this.marked[f] = fields[f].map(() => false); // creates an array of same length as that row in fields
		}
	}

	mark(number) {
		for (let y = 0; y < this.fields.length; y++) {
			for (let x = 0; x < this.fields[y].length; x++) {
				if (this.fields[y][x] === number) {
					this.marked[y][x] = true;
					// console.log(`marking number ${number} at ${x}/${y}`);
				}
			}
		}
	}

	hasWon() {
		return this.checkColumns() || this.checkRows();
	}

	checkColumns() {
		for (let x = 0; x < this.marked[0].length; x++) {
			let success = true;
			for (let y = 0; y < this.marked.length; y++) {
				if (!this.marked[y][x]) {
					success = false;
				}
			}
			if (success) { return true; }
		}
		return false;
	}

	checkRows() {
		for (let row of this.marked) {
			let success = true;
			for (let field of row) {
				if (!field) {
					success = false;
				}
			}
			if (success) { return true; }
		}
		return false;
	}

	toString() {
		return this.fields.map((row, y) => {
			return row.map((field, x) => {
				let m = this.marked[y][x] ? '*' : ' ';
				return `${m}${field}${m}`;
			}).join('\t');
		}).join('\n');
	}

	getUnmarked() {
		const unmarked = [];
		this.fields.forEach((row, y) => {
			row.forEach((field, x) => {
				if (!this.marked[y][x]) {
					unmarked.push(field);
				}
			});
		});
		return unmarked;
	}

	calculateScoreWith(finalDraw) {
		const unmarked = this.getUnmarked();
		const sum = unmarked.reduce((prev, current) => prev+current);
		return sum * finalDraw;
	}
}
