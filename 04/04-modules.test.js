import assert, { deepStrictEqual, strictEqual } from 'assert';

import { describe, it } from 'mocha';

import { loadRelativeFile } from '../00-general/general.js';
import { Board, parse } from './04-modules.js';

import demoInputParsed from './04-example.js';

const demoInput = await loadRelativeFile(import.meta.url, '04-example.txt');

describe('04-module', () => {
	it('parses the input', async () => {
		const actual = parse(demoInput);
		deepStrictEqual(actual, demoInputParsed);
	});

	it('board recognizes a row', () => {
		const b = new Board([
			[1, 2, 3],
			[4, 5, 6],
			[7, 8, 9]
		]);

		assert(!b.checkRows());
		assert(!b.checkColumns());
		assert(!b.hasWon());
		b.mark(2);
		b.mark(5);
		b.mark(8);
		assert(b.checkColumns());
		assert(!b.checkRows());
		assert(b.hasWon());

		b.mark(4);
		b.mark(6);
		assert(b.checkColumns());
		assert(b.checkRows());
		assert(b.hasWon());
	});

	it('board recognizes a column', () => {
		const b = new Board([
			[1, 2, 3],
			[4, 5, 6],
			[7, 8, 9]
		]);

		assert(!b.checkRows());
		assert(!b.checkColumns());
		assert(!b.hasWon());
		b.mark(4);
		b.mark(5);
		b.mark(6);
		assert(b.checkRows());
		assert(!b.checkColumns());
		assert(b.hasWon());
		
		b.mark(2);
		b.mark(8);
		assert(b.checkRows());
		assert(b.checkColumns());
		assert(b.hasWon());
	});

	it('get unmarked', () => {
		const b = new Board([
			[1, 2, 3],
			[4, 5, 6],
			[7, 8, 9]
		]);

		deepStrictEqual(b.getUnmarked().sort(), [1,2,3,4,5,6,7,8,9]);

		b.mark(1);
		b.mark(9);
		deepStrictEqual(b.getUnmarked().sort(), [2,3,4,5,6,7,8]);
	});

	it('recognizes winning', () => {
		const data = parse(demoInput);
		const boards = data.boards.map(b => new Board(b));

		for (let i = 0; i < 11; i++) {
			const drawnNumber = data.drawNumbers[i];
			for (let b of boards) {
				b.mark(drawnNumber);
				assert(!b.hasWon(), `board should not have won, yet:\n${b.toString()}`);
			}
		}

		for (let i in boards) {
			// making sure none of these have won, yet!
			assert(!boards[i].hasWon(), `board ${i} should not have won, yet:\n${boards[i].toString()}`);

			// drawing that next number
			boards[i].mark(data.drawNumbers[11]);
		}

		assert(!boards[0].hasWon());
		assert(!boards[1].hasWon());
		assert(boards[2].hasWon());
	});


	it('calculate winning points', () => {
		const data = parse(demoInput);
		const boards = data.boards.map(b => new Board(b));

		for (let i = 0; i < data.drawNumbers.length; i++) {
			const drawnNumber = data.drawNumbers[i];
			for (let b of boards) {
				b.mark(drawnNumber);
				if (b.hasWon()) {
					const points = b.calculateScoreWith(drawnNumber);
					strictEqual(points, 4512);
					return;
				}
			}
		}
		assert(false, 'we should never end up here, because one board should have won');
	});
});
