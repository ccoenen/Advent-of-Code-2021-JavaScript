import { loadRelativeFile } from '../00-general/general.js';

const input = await loadRelativeFile(import.meta.url, '03-input.txt');
const values = input.split('\n');

function bitHistogram(values) {
	const gammaAnalysis = [
		// [] <- 1 array per digit
		// [23, 42] <- arrays will contain counts for off (index 0) or on (index 1)
	];

	for (const value of values) {
		for (let i = 0; i < value.length; i++) {
			if (!gammaAnalysis[i]) { gammaAnalysis[i] = [0, 0]; }

			gammaAnalysis[i][value[i]]++;
		}
	}
	return gammaAnalysis;
}


const gammaAnalysis = bitHistogram(values);

let gammaRaw = '';
let epsilonRaw = '';
gammaAnalysis.forEach(digit => {
	gammaRaw += digit[0] > digit[1] ? '0' : '1';
	epsilonRaw += digit[0] > digit[1] ? '1' : '0';
});

let gamma = parseInt(gammaRaw, 2);
let epsilon = parseInt(epsilonRaw, 2);

console.log(`gamma: ${gamma}, epsilon: ${epsilon}, combined: ${gamma*epsilon}`);

// Part 2
let filteredValuesO2 = [].concat(values); // cloning the array
for (let i = 0; i < values[0].length; i++) {
	const analysis = bitHistogram(filteredValuesO2);
	filteredValuesO2 = filteredValuesO2.filter((value) => {
		const expected = analysis[i][0] > analysis[i][1] ? '0' : '1';
		return value[i] === expected;
	});
	if (filteredValuesO2.length === 1) break;
	console.log(`filteredValuesO2 now has ${filteredValuesO2.length} items`);
}

let filteredValuesCO2 = [].concat(values); // cloning the array
for (let i = 0; i < values[0].length; i++) {
	const analysis = bitHistogram(filteredValuesCO2);
	filteredValuesCO2 = filteredValuesCO2.filter((value) => {
		const expected = analysis[i][0] <= analysis[i][1] ? '0' : '1';
		return value[i] === expected;
	});
	if (filteredValuesCO2.length === 1) break;
	console.log(`filteredValuesCO2 now has ${filteredValuesCO2.length} items`);
}

console.log(`${filteredValuesO2}, ${filteredValuesCO2}, ${parseInt(filteredValuesO2[0], 2) * parseInt(filteredValuesCO2, 2)}`);
